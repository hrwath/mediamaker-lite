#!/usr/bin/env bash

export SECRET_KEY_BASE=$(date | openssl sha1 -hmac "booperdooper")
export RAILS_ENV=production

cd /vagrant

# setup project
echo "[SETUP] Installing gems..."
bundle install

# setup database
echo "[SETUP] Setting up database..."
bundle exec rake db:create >/dev/null
bundle exec rake db:migrate >/dev/null

# start the server
echo "[SETUP] Booting Puma server..."
rails server puma -b 192.168.111.111 -p 9292 -d

# start Resque
echo "[SETUP] Starting Resque..."
PIDFILE=./tmp/pids/resque.pid BACKGROUND=yes TERM_CHILD=1 QUEUE=* \
    bundle exec rake environment resque:work

# start Resque Scheduler
echo "[SETUP] Starting Resque Scheduler..."
PIDFILE=./tmp/pids/resque-scheduler.pid LOGFILE=./log/resque-scheduler.log BACKGROUND=yes \
    bundle exec rake environment resque:scheduler

echo
echo
echo "Setup is complete!

Application is available at http://192.168.111.111:9292
Worker console is available at http://192.168.111.111:9292/resque"