# Setup #

Clone the repository:

```
$ git clone https://bitbucket.org/hrwath/mediamaker-lite.git
```

Setup Vagrant box:

```
$ cd mediamaker-lite
$ vagrant up
```

Setup the application:

```
$ vagrant ssh
$ cd /vagrant
$ bash setup.sh
```

After the setup is complete application will be accessible at http://192.168.111.111:9292 and Resque console at [http://192.168.111.111:9292/resque](http://192.168.111.111:9292/resque).