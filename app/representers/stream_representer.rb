module StreamRepresenter
  include Roar::JSON
  
  property :id  
  property :src  
  property :type  
end
