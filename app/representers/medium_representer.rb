module MediumRepresenter
  include Roar::JSON
  include MediumLiteRepresenter

  collection :images, extend: ImageRepresenter
  collection :streams, extend: StreamRepresenter
end
