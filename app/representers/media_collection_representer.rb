module MediaCollectionRepresenter
  include Roar::JSON
  include Roar::Hypermedia
  include Roar::JSON::HAL

  property :total, getter: lambda { |opts| opts[:total] }
  property :limit, getter: lambda { |opts| opts[:limit] }
  property :offset, getter: lambda { |opts| opts[:offset] }

  collection :media, extend: MediumLiteRepresenter

  link :self do |opts|
    opts[:href]
  end

  def media
    represented
  end
end
