module ImageRepresenter
  include Roar::JSON
  
  property :id  
  property :src  
  property :width  
  property :height  
end
