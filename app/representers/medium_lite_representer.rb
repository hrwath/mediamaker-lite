module MediumLiteRepresenter
  include Roar::JSON
  include Roar::Hypermedia
  include Roar::JSON::HAL

  property :id
  property :title
  property :description

  link :self do |opts|
    opts[:href]
  end
end