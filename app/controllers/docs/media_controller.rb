module Docs
  class MediaController
    include Swagger::Blocks

    swagger_path '/media' do
      operation :get do
        key :description, 'Return list of media'
        key :tags, ['media']
        key :produces, ['application/json']

        parameter do
          key :name, :limit
          key :in, :query
          key :description, 'Number of returned media'
          key :default, 8
          key :type, :integer
        end

        parameter do
          key :name, :offset
          key :in, :query
          key :description, 'List offset'
          key :default, 0
          key :type, :integer
        end

        parameter do
          key :name, :sort
          key :in, :query
          key :description, 'Sort parameter'
          key :type, :string
        end

        parameter do
          key :name, :order
          key :in, :query
          key :description, 'Sorting direction'
          key :default, 'asc'
          key :type, :string
        end

        response 200 do
          key :description, 'List of media'
          schema do
            key :'$ref', :Media
          end
        end
      end

      operation :post do
        key :description, 'Create new medium'
        key :tags, ['media']

        parameter do
          key :name, :sessiontoken
          key :required, true
          key :in, :header
          key :description, 'Session token'
          key :type, :string
        end

        parameter do
          key :name, :medium
          key :in, :body
          key :description, 'Medium to add'
          schema do
            key :'$ref', :Medium
          end
        end

        response 201 do
          key :description, 'Medium created'
        end

        response 401 do
          key :description, 'Unauthorized'
        end
      end

      operation :put do
        key :description, 'Update medium'
        key :tags, ['media']

        parameter do
          key :name, :sessiontoken
          key :required, true
          key :in, :header
          key :description, 'Session token'
          key :type, :string
        end

        parameter do
          key :name, :medium
          key :in, :body
          key :description, 'Medium to update'
          schema do
            key :'$ref', :MediumUpdate
          end
        end

        response 201 do
          key :description, 'Medium updated'
        end

        response 401 do
          key :description, 'Unauthorized'
        end
      end
    end

    swagger_path '/media/{id}' do
      operation :get do
        key :description, 'Get single medium'
        key :tags, ['media']

        parameter do
          key :name, :id
          key :required, true
          key :in, :path
          key :description, 'ID of medium'
          key :type, :integer
        end

        response 200 do
          key :description, 'Single medium'
          schema do
            key :'$ref', :Medium
          end
        end

        response 404 do
          key :description, 'Medium not found'
        end
      end

      operation :delete do
        key :description, 'Delete medium'
        key :tags, ['media']

        parameter do
          key :name, :sessiontoken
          key :required, true
          key :in, :header
          key :description, 'Session token'
          key :type, :string
        end

        parameter do
          key :name, :id
          key :required, true
          key :in, :query
          key :description, 'ID of medium'
          key :type, :integer
        end

        response 204 do
          key :description, 'Medium deleted'
        end

        response 401 do
          key :description, 'Unauthorized'
        end
      end
    end
  end
end