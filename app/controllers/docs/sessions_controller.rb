module Docs
  class SessionsController
    include Swagger::Blocks

    swagger_path '/login' do
      operation :post do
        key :description, 'User login'
        key :tags, ['login']
        key :produces, ['application/json']

        parameter do
          key :name, :user
          key :in, :body
          key :description, 'User object'
          schema do
            key :'$ref', :User
          end
        end

        response 200 do
          key :description, 'Succesful login response'
          schema do
            key :'$ref', :Token
          end
        end

        response 401 do
          key :description, 'Unauthorized'
        end
      end
    end

    swagger_path '/logout' do
      operation :post do
        key :description, 'User logout'
        key :tags, ['login']

        parameter do
          key :name, :sessiontoken
          key :in, :header
          key :description, 'Session token'
          key :required, true
          key :type, :string
        end

        response 204 do
          key :description, 'No content'
        end

        response 401 do
          key :description, 'Unauthorized'
        end
      end
    end
  end
end