class ImportsController < ApplicationController
  include RenderingHelper

  def ingest
    params[:_json].each do |uri|
      Resque.enqueue(IngestImport, uri)
    end

    render_created
  end
end
