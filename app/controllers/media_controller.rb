class MediaController < ApplicationController
  include Roar::Rails::ControllerAdditions

  include CorsHelper
  include SessionsHelper
  include RenderingHelper

  respond_to :json

  before_action :preflight_check
  after_action :respond_for_options

  # get medium from ActiveRecord
  before_action :set_medium, only: [:show, :update, :destroy]

  # standarize params to autosave nested models using accepts_nested_parameters_for in models
  before_action :standarize_params, only: :create

  # check for authorization in particular actions
  before_action :check_authorization, only: [:create, :update, :destroy]

  # show 404 JSON when record not found
  rescue_from ActiveRecord::RecordNotFound, with: :render_not_found

  # GET /media
  def index
    p = params.merge Rails.application.config.x.default_params do |key, old, new|
      old ? old : new
    end

    params = ActionController::Parameters.new(p)

    @media = Medium.limit(params[:limit]).offset(params[:offset]).order "#{params[:sort]} #{params[:order]}"
    respond_with @media,
                 :represent_with => MediaCollectionRepresenter,
                 :href => "#{media_url}?#{params.merge(request.GET).except(:controller, :action, :format).to_query}",
                 :limit => params[:limit].to_i,
                 :offset => params[:offset].to_i,
                 :total => Medium.count
  end

  # GET /media/1
  def show
    respond_with @medium, :href => medium_url
  end

  # POST /media
  def create
    @medium = Medium.new(medium_params)
    @medium.save ? render_created : render_errors(@medium)
  end

  # PUT /media/1
  def update
    if @medium.update_attributes title: params[:title], description: params[:description]
      render_no_content
    else
      render_errors @medium
    end
  end

  # DELETE /media/1
  def destroy
    @medium.destroy
    render_no_content
  end

  private

  def set_medium
    @medium = Medium.find(params[:id])
  end

  # permitted parameters for medium object
  def medium_params
    params.require(:medium).permit(:id, :title, :description,
                                   :images_attributes => [:id, :src, :width, :height],
                                   :streams_attributes => [:id, :src, :type])
  end

  def check_authorization
    render_unauthorized unless session_valid?
  end

  def standarize_params
    params[:medium][:images_attributes]  = params[:images]  if params.has_key? :images
    params[:medium][:streams_attributes] = params[:streams] if params.has_key? :streams
  end
end
