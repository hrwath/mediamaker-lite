class SessionsController < ApplicationController
  include CorsHelper
  include SessionsHelper
  include RenderingHelper

  before_action :preflight_check
  after_action :respond_for_options

  before_action :validate_params, only: :create

  # POST /login
  def create
    user = { username: Rails.application.config.x.authenticate.username,
             password: Rails.application.config.x.authenticate.password }

    if user_authenticated?(user)
      log_in
      render json: { token: session.id }
    else
      render_unauthorized
    end
  end

  # POST /logout
  def destroy
    if session_valid?
      log_out
      render_no_content
    else
      render_unauthorized
    end
  end

  # handle unpermitted params by raising an exception
  ActionController::Parameters.action_on_unpermitted_parameters = :raise

  # rescue from exception by rendering JSON with errors
  rescue_from(ActionController::UnpermittedParameters) do |pme|
    render json: { error: {unknown_parameters: pme.params} }, status: :bad_request
  end

  private

  def validate_params
    session.delete :init
    user = Validates::User.new(params)

    render json: { errors: user.errors } unless user.valid?
  end

  def user_authenticated?(user)
    user == { username: params[:username], password: params[:password] }
  end
end
