module Validates
  class User
    include ActiveModel::Validations

    attr_accessor :username, :password

    validates :username, presence: true
    validates :password, presence: true

    def initialize(params={})
      @username = params[:username]
      @password = params[:password]

      ActionController::Parameters.new(params).require(:session).permit(:username, :password)
    end
  end
end