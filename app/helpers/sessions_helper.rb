module SessionsHelper
  # create the session
  def log_in
    session[:user_id] = session.id
  end

  # remove the session
  def log_out
    session[:user_id] = nil
  end

  def session_valid?
    if request.headers['sessiontoken']
      request.session_options[:id] = request.headers['sessiontoken']

      !session.blank? && session[:user_id] == request.headers['sessiontoken']
    else
      false
    end
  end
end
