module RenderingHelper
  def render_errors(obj)
    render json: { error: obj.errors }, status: :bad_request
  end

  def render_ok
    render nothing: true, status: :ok
  end

  def render_created
    render nothing: true, status: :created
  end

  def render_no_content
    render nothing: true, status: :no_content
  end

  def render_unauthorized
    render nothing: true, status: :unauthorized
  end

  def render_not_found
    render nothing: true, status: :not_found
  end
end