module CorsHelper
  def respond_for_options
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, DELETE, OPTIONS'
    headers['Access-Control-Max-Age'] = '1728000'

    head :no_content if request.method == 'OPTIONS'
  end

  def preflight_check
    headers['Access-Control-Allow-Headers'] = 'X-Requested-With, X-Prototype-Version, Content-Type, sessiontoken'
    respond_for_options
  end
end