class Token
  include Swagger::Blocks

  swagger_schema :UserOutput do
    property :token do
      key :type, :string
    end
  end
end