class Media < ActiveRecord::Base
  include Swagger::Blocks

  swagger_schema :Media do
    property :total do
      key :type, :integer
    end

    property :limit do
      key :type, :integer
    end

    property :offset do
      key :type, :integer
    end

    property :media do
      key :type, :array
      items do
        key :'$ref', :MediaItem
      end
    end
  end

  swagger_schema :MediaItem do
    property :id do
      key :type, :string
    end
    property :title do
      key :type, :string
    end
    property :description do
      key :type, :string
    end
  end
end