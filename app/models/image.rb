class Image < ActiveRecord::Base
  include Swagger::Blocks

  belongs_to :medium

  swagger_schema :Image do
    property :src do
      key :type, :string
    end

    property :width do
      key :type, :integer
    end

    property :height do
      key :type, :integer
    end
  end
end
