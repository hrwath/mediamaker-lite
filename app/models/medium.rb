class Medium < ActiveRecord::Base
  include Swagger::Blocks

  has_many :images, dependent: :destroy
  has_many :streams, dependent: :destroy

  accepts_nested_attributes_for :images, allow_destroy: true
  accepts_nested_attributes_for :streams, allow_destroy: true

  validates :title, presence: true

  swagger_schema :Medium do
    key :required, [:title]

    property :title do
      key :type, :string
    end

    property :description do
      key :type, :string
    end

    property :images do
      key :type, :array
      items do
        key :'$ref', :Image
      end
    end

    property :streams do
      key :type, :array
      items do
        key :'$ref', :Stream
      end
    end
  end

  swagger_schema :MediumUpdate do
    key :required, [:title]

    property :title do
      key :type, :string
    end

    property :description do
      key :type, :string
    end
  end
end
