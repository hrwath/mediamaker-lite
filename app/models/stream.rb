class Stream < ActiveRecord::Base
  include Swagger::Blocks

  belongs_to :medium

  # 'type' is a reserved word in ActiveRecord, so line below is needed to prevent 500 error
  self.inheritance_column = nil

  swagger_schema :Stream do
    property :src do
      key :type, :string
    end

    property :type do
      key :type, :string
    end
  end
end
