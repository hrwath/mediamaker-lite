require 'net/http'
require 'json'

class IngestImport
  @queue = :ingest

  def self.perform
    uri = URI('http://example-api-lite.getsandbox.com/ingest/fuhu/media/videos')
    response = Net::HTTP.get(uri)

    input = JSON.parse response

    input['list'].each do |djamet|
      external_id = djamet['external_id']
      imported = Medium.find_by_external_id(external_id)

      if imported.nil?
        title = djamet['titles']['default']
        description = djamet['descriptions']['default']

        @medium = Medium.new(title: title, description: description, external_id: external_id)

        djamet['images'].each do |image|
          if image['formats'].count > 0
            _image = image['formats']['original_size']
            @image = Image.new(src: _image['src'], width: _image['width'], height: _image['height'])

            @medium.images << @image
          end
        end

        djamet['streams']['wvm'].each do |stream|
          @stream = Stream.new(src: stream['src'], type: stream['type'])

          @medium.streams << @stream
        end

        @medium.save
      end
    end
  end
end