require 'resque/server'
require 'resque/scheduler/server'

Rails.application.routes.draw do
  post 'imports/ingest'

  match 'login', to: 'sessions#create', via: [:options]
  post 'login' => 'sessions#create'

  match 'logout', to: 'sessions#destroy', via: [:options]
  post 'logout' => 'sessions#destroy'

  scope path: '/media', controller: :media, defaults: { :format => 'json' } do
    match '', to: 'media#index', via: [:options]
    match ':id', to: 'media#show', via: [:options]

    get '' => :index, as: :media
    get ':id' => :show, as: :medium
    post '' => :create
    put ':id' => :update
    delete ':id' => :destroy
  end

  mount Resque::Server.new => '/resque'

  # Swagger
  get '/api' => redirect('/swagger/dist/index.html?url=/apidocs')
  resources :apidocs, only: [:index]
end
