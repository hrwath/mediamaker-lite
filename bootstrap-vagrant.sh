#!/usr/bin/env bash

# pl_PL.UTF-8
sudo locale-gen pl_PL.UTF-8
sudo update-locale LC_ALL=pl_PL.UTF-8

# Default MySQL root password
debconf-set-selections <<< 'mysql-server mysql-server/root_password password vagrant'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password vagrant'

# Redis
sudo apt-add-repository ppa:chris-lea/redis-server

sudo apt-get install -y mysql-server mysql-client 2> /dev/null
sudo apt-get install -y redis-server libmysqlclient-dev

# disable MySQL root password
sudo mysqladmin -u root -pvagrant password ''

# RVM and Ruby
su -c 'bash /vagrant/bootstrap-rvm.sh' vagrant