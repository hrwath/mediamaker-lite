#!/usr/bin/env bash

echo "[HALT] Shutting down Puma server..."
kill -9 $(cat tmp/pids/server.pid)

echo "[HALT] Shutting down Resque..."
kill -9 $(cat tmp/pids/resque.pid)

echo "[HALT] Shutting down Resque Scheduler..."
kill -9 $(cat tmp/pids/resque-scheduler.pid)