class CreateStreams < ActiveRecord::Migration
  def change
    create_table :streams do |t|
      t.string :src
      t.string :type
      t.integer :medium_id

      t.timestamps null: false
    end
  end
end
