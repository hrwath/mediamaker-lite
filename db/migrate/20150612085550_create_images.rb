class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :src
      t.integer :width
      t.integer :height
      t.integer :medium_id

      t.timestamps null: false
    end
  end
end
