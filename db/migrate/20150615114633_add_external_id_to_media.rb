class AddExternalIdToMedia < ActiveRecord::Migration
  def change
    change_table :media do |t|
      t.string :external_id
      t.index :external_id
    end
  end
end
