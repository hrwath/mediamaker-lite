#!/usr/bin/env bash

command curl -sSL https://rvm.io/mpapis.asc | gpg --import -

cd /home/vagrant
\curl -L https://get.rvm.io | bash -s stable

source /home/vagrant/.profile
source /home/vagrant/.rvm/scripts/rvm

rvm install 2.2.0
rvm use 2.2.0 --default

gem install bundler --no-ri --no-rdoc